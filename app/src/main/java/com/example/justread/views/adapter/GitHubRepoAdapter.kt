package com.example.architecturetask.views.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.architecturetask.R
import com.example.architecturetask.model.Repo
import java.util.*

class GitHubRepoAdapter : RecyclerView.Adapter<GitHubRepoAdapter.StarRepoViewHolder>() {

    private var repos: List<Repo> = ArrayList<Repo>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): StarRepoViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.stars_item, null)
        return StarRepoViewHolder(view)
    }

    override fun onBindViewHolder(holder: StarRepoViewHolder, position: Int) {
        holder.repoName.text = repos[position].repoName
        holder.repoStars.text = "${repos[position].starCount} stars"

        if (repos[position].description == null || repos[position].description!!.length === 0)
            holder.repoDescription.text = "No Description"
        else
            holder.repoDescription.text = repos[position].description

        if (repos[position].language == null || repos[position].language!!.length === 0)
            holder.repoDescription.text = "No Language"
        else
            holder.repoLanguage.text = repos[position].language
    }

    override fun getItemCount(): Int {
        return repos.size
    }

    fun setReposList(repos: List<Repo>) {
        this.repos = repos
        notifyDataSetChanged()
    }

    inner class StarRepoViewHolder(itemView: View) :
        ViewHolder(itemView) {
        var repoName: TextView
        var repoDescription: TextView
        var repoLanguage: TextView
        var repoStars: TextView

        init {
            repoName = itemView.findViewById(R.id.repoName)
            repoDescription = itemView.findViewById(R.id.description)
            repoLanguage = itemView.findViewById(R.id.language)
            repoStars = itemView.findViewById(R.id.starsCount)
        }
    }
}

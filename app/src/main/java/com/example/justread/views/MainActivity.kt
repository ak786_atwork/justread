package com.example.architecturetask.views

import android.os.Bundle
import com.example.architecturetask.R
import com.example.justread.views.BaseActivity

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
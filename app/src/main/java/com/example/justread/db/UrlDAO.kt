package com.example.justread.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.justread.model.Url

@Dao
interface UrlDAO {
    @Query("SELECT * FROM url ")
    suspend fun fetchAll(): List<Url>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUrl(url: Url)
}

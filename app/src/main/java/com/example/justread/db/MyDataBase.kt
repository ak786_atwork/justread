package com.example.justread.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.justread.model.Url


@Database(entities = [Url::class], version = 1)
abstract class MyDataBase : RoomDatabase() {
    abstract val urlDAO: UrlDAO

    companion object {
        var database: MyDataBase? = null
        fun getInstance(context: Context?): MyDataBase {

            if (database == null) {
                synchronized(this) {
                    if (database == null) {
                        database = Room.databaseBuilder(
                            context!!,
                            MyDataBase::class.java, "url_note.db"
                        ).fallbackToDestructiveMigration()
                            .build()
                    }
                }

            }
            return database!!
        }
    }
}

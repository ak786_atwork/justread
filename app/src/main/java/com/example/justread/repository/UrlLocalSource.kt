package com.example.justread.repository

import com.example.justread.MyApplication
import com.example.justread.db.MyDataBase
import com.example.justread.model.Url

class UrlLocalSource {

    suspend fun fetchAll(): List<Url> {
        return MyDataBase.getInstance(MyApplication.context).urlDAO.fetchAll()
    }

    suspend fun insertUrl(url: Url) {
        MyDataBase.getInstance(MyApplication.context).urlDAO.insertUrl(url)
    }

}

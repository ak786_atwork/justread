package com.example.justread.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.justread.model.Url
import com.example.justread.repository.UrlLocalSource
import kotlinx.coroutines.launch


class UrlViewModel : ViewModel() {

    private val repoLiveData: MutableLiveData<List<Url>> = MutableLiveData<List<Url>>()
    private val urlLocalSource = UrlLocalSource()

    fun fetchAll() {
        viewModelScope.launch {
            repoLiveData.postValue(urlLocalSource.fetchAll())
        }
    }

    fun insertUrl(url: Url) {
        viewModelScope.launch {
            urlLocalSource.insertUrl(url)
        }
    }

}
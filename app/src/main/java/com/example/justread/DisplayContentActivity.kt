package com.example.justread

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import androidx.appcompat.app.ActionBar
import java.util.*

class DisplayContentActivity : AppCompatActivity() {

    private lateinit var webView: WebView
    private lateinit var progressBar: ProgressBar


    private val webViewClient = object : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
            progressBar.visibility = View.GONE

            super.onPageFinished(view, url)
        }

        override fun shouldOverrideUrlLoading(webView: WebView?, url: String): Boolean {
            webView?.loadUrl(url)
            progressBar.visibility = View.VISIBLE

            return true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_display_content)
        setUpProgressBar()

        webView = findViewById(R.id.webview)
        webView.webViewClient = webViewClient
        webView.loadUrl("https://medium.com/tag/android")

    }

    private fun setUpProgressBar() {
        progressBar = findViewById(R.id.progress_circular)
        progressBar.progress = 0
    }


    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            showAlertDialog()

            super.onBackPressed()
        }
    }

    private fun showAlertDialog() {
    }



}
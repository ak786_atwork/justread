package com.example.justread.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "url")
data class Url(
    @PrimaryKey(autoGenerate = true)

    @NonNull
    @ColumnInfo(name = "url")
    val url : String,

    @ColumnInfo(name = "isFavourite")
    val isFavourite: Boolean = false,

    @ColumnInfo(name = "showOnMainPage")
    val showOnMainPage: Boolean = false,

    @ColumnInfo(name = "isAvailableOffline")
    val isAvailableOffline: Boolean = false,

    @ColumnInfo(name = "isAuthorFollowed")
    val isAuthorFollowed: Boolean = false,

    @ColumnInfo(name = "isUrlRemoved")
    val isUrlRemoved: Boolean = false,

    @ColumnInfo(name = "authorName")
    val authorName: String = "",

    @ColumnInfo(name = "tag")
    val tag: String = "",

    @ColumnInfo(name = "readCount")
    val readCount: Int = 0,

    @ColumnInfo(name = "priority")
    val priority: Int = 0,

    @ColumnInfo(name = "mustReadCount")
    val mustReadCount: Int = 0

)

